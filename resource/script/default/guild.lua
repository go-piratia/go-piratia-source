GUILD_TYPE_NAVY = 0
GUILD_TYPE_PIRATE = 1

guild_mt = {}

function guild_mt.__index (t,key)
	if 	key == "name" then
		rawset(t, key, GetGuildName(t.id))
	end
	return rawget(t, key)
end

function guild_mt.__newindex (t, key, value)
	if key == "name" then
		print("Guild Structure: Can't set name of guild.")
	else
		rawset(t, key, value)
	end
end

function guild_wrap(id)
	guild = {}
	guild.id = id

	setmetatable(guild, guild_mt)
	return id
end