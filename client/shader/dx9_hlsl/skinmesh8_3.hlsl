//HLSL with constrained registers adapted to PKO shaders syntax
//Converting ASM to vs_1_1 accepted by DX8:
//  Change "vs_1_1" to "vs.1.1".
//  Comment all dcl_ instructions (// or ;) - these are already defined in code.
//  Change all c#[?] to c[? + #].

//c# Constants
uint4 Base : register(c0);
float4x4 World : register(c1);
float4 LightDirection : register(c5);
float4 Ambient : register(c6);
float4 Diffuse : register(c7);
float4 BV[75] : register(c21); // 75/3=25 bone matrixes on vs_1_1
//max is 96 c registers in vs_1_1.

struct VertexShaderInput { //v# Input parameters
    float4 Position : POSITION0 : register(v0);
	float3 Blendweight : BLENDWEIGHT0 : register(v1);
	uint4 Blendindices : BLENDINDICES0 : register(v2);
	float3 Normal : NORMAL0 : register(v3);
	//float4 Color : COLOR0 : register(v5);
	float2 TexCoord0 : TEXCOORD0 : register(v7);
	//float4 TexCoord1 : TEXCOORD1 : register(v8);
};
struct VertexShaderOutput { //o# Output parameters
    float4 Position : POSITION0 : register(oP);
	float4 Color : COLOR0 : register(oD0);
	float4 Specular : COLOR1 : register(oD1);
	float2 TexCoord0 : TEXCOORD0 : register(oT0);
	//float4 TexCoord1 : TEXCOORD1 : register(oT1);
};
VertexShaderOutput main(VertexShaderInput input) {
    VertexShaderOutput output;
	const int blends = 3;
	float3 blendPos = float3(0,0,0);
	float3 norm = float3(0,0,0);
	float3 blendweights = input.Blendweight;
	blendweights[blends-1] = Base.x-dot(blendweights,Base.x);
	for (int i = 0; i < blends; ++i) {
		uint j = input.Blendindices[i]*Base.w;
		float4x3 BV4x3 = { BV[j].x, BV[j+1].x, BV[j+2].x,
		                   BV[j].y, BV[j+1].y, BV[j+2].y,
		                   BV[j].z, BV[j+1].z, BV[j+2].z,
		                   BV[j].w, BV[j+1].w, BV[j+2].w };
		blendPos += blendweights[i] * mul(input.Position, BV4x3);
		norm += blendweights[i] * mul(input.Normal, BV4x3);
	}
    output.Position = mul(float4(blendPos,Base.x),World);
	output.Color = min((lit(dot(normalize(norm),LightDirection),input.Blendindices.y*Base.y,input.Blendindices.w*Base.w).y*Diffuse)+Ambient,Base.x);
	output.Specular = Base.z;
	output.TexCoord0 = input.TexCoord0;
    return output;
};