//HLSL with constrained registers adapted to PKO shaders syntax
//Converting ASM to vs_1_1 accepted by DX8:
//  Change "vs_1_1" to "vs.1.1".
//  Comment all dcl_ instructions (// or ;) - these are already defined in code.
//  Change all c#[?] to c[? + #].

//c# General constants
float4x4 World : register(c0);
float4x4 ViewProjection : register(c4);

//Specified constants in SmallMap.h
float4 Alpha : register(c8);
float4 UV[80] : register(c9); //Unknown amount

struct VertexShaderInput { //v# Input parameters
    //float4 Position : POSITION0 : register(v0);
	float Blendweight : BLENDWEIGHT0 : register(v1);
	//uint Blendindices : BLENDINDICES0 : register(v2);
	//float4 Color : COLOR0 : register(v5);
	float4 TexCoord0 : TEXCOORD0 : register(v7);
	//float4 TexCoord1 : TEXCOORD1 : register(v8);
};
struct VertexShaderOutput { //o# Output parameters
    float4 Position : POSITION0 : register(oP);
	float4 Color : COLOR0 : register(oD0);
	float4 TexCoord0 : TEXCOORD0 : register(oT0);
};
VertexShaderOutput main(VertexShaderInput input) {
    VertexShaderOutput output;
    float4 worldPosition = mul(UV[input.Blendweight], World);
    output.Position = mul(worldPosition, ViewProjection); //2D
	output.Color = Alpha;
	output.TexCoord0 = input.TexCoord0;
    return output;
}